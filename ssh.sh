#!/usr/bin/env bash
# Paulo Aleixo Campos
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
function shw_info { echo -e '\033[1;34m'"$1"'\033[0m'; }
function error { echo "ERROR in ${1}"; exit 99; }
trap 'error $LINENO' ERR
#set -o errexit
  # NOTE: the "trap ... ERR" alreay stops execution at no error, even when above line is commente-out
set -o pipefail
set -o nounset
#set -o xtrace
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

cd $__dir
ssh -F ./portable_dotSsh/ssh_config "$@"
