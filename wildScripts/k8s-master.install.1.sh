#!/usr/bin/env bash
# Paulo Aleixo Campos
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__dbg_on_off=on  # on off
function shw_info { echo -e '\033[1;34m'"$1"'\033[0m'; }
function error { echo "ERROR in ${1}"; exit 99; }
trap 'error $LINENO' ERR
function dbg { [[ "$__dbg_on_off" == "on" ]] || return; echo -e '\033[1;34m'"dbg $(date +%Y%m%d%H%M%S) ${BASH_LINENO[0]}\t: $@"'\033[0m';  }
#set -o errexit
set -o pipefail
set -o nounset
#set -o xtrace

[[ "$(id -u)" == 0 ]] || { echo "Aborting - run me as root"; exit 1; }

YYYYMMDDhhmmss=$(date +%Y%m%d%H%M%S)

setenforce 0

sed 's_SELINUX=enforcing_SELINUX=permissive_g' -i."${YYYYMMDDhhmmss}".bak /etc/selinux/config

systemctl disable firewalld && systemctl stop firewalld

cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

yum install -y docker kubelet kubeadm kubectl kubernetes-cni
systemctl enable docker && systemctl start docker
systemctl enable kubelet && systemctl start kubelet

sysctl -w net.bridge.bridge-nf-call-iptables=1
echo "net.bridge.bridge-nf-call-iptables=1" > /etc/sysctl.d/k8s.conf
swapoff -a && sed  '/ swap / s/^/#/' -i."${YYYYMMDDhhmmss}".bak /etc/fstab


echo "All done, and ready to be cloned - shutting down in 10secs"
for i in $(seq 1 10); do echo -n + ; sleep 1; done
echo 
shutdown now

