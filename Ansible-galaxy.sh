#!/usr/bin/env bash
function shw_info { echo -e '\033[1;34m'"$1"'\033[0m'; }
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
clear
date '+%F_%T'
echo "$0 $@"
export ANSIBLE_FORCE_COLOR=true
time python ansible-galaxy "$@"
