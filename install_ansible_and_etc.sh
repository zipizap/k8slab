#!/usr/bin/env bash
# Paulo Aleixo Campos
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
function shw_info { echo -e '\033[1;34m'"$1"'\033[0m'; }
function error { echo "ERROR in ${1}"; exit 99; }
trap 'error $LINENO' ERR
#set -o errexit
  # NOTE: the "trap ... ERR" alreay stops execution at any error, even when above line is commente-out
set -o pipefail
set -o nounset
#set -o xtrace

shw_info "+ Download portable-ansible"
cd "${__dir}"
(curl -sSL 'https://github.com/ownport/portable-ansible/releases/download/v0.4.1/portable-ansible-v0.4.1-py2.tar.bz2' | tar xjvf - ) | sed 's/^.*$/+/g' | tr -d '\n' ; echo 

#shw_info "+ Setting internal vars"
#SSH_CONFIG_FILE="${__dir}"/portable_dotSsh/ssh_config
#cd "${__dir}"
#grep __THIS_DIR__  "${SSH_CONFIG_FILE}" &>/dev/null && sed "s.__THIS_DIR__.$__dir/portable_dotSsh.g" -i.orig "${SSH_CONFIG_FILE}"

shw_info "+ All done"
exit 0

